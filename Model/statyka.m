clear;
load('dane.mat');

delta_o = [-10, 0, 10, 20];
Ch = [0.22,0.09,-0.04,-0.18];

c_av = 1.047; %m
Sf = 1.7; %m
h = 25000;
rho = 1.225*(1-(h/44300))^4.256;
l_Fr0=0.08; %m

%% l(delta)
% C = [-394.7; 291.82];
% B_0 = [14.7; 81.88];
yB = -1;
for i = 1:4
    Bx = B_0(1)*cosd(delta_o(i))+B_0(2)*sind(delta_o(i));
    By = -B_0(1)*sind(delta_o(i))+B_0(2)*cosd(delta_o(i));
    a = (C(2)-By)/(C(1)-Bx);
    b = By - a*Bx;
    
    l_Fr(i) = abs(b)/sqrt(a^2+(-1)^2)*10^-3;
end

n = 300;
v = linspace(95,800,n);

for i = 1:4;
    for j = 1:n
        Q(j,i) = Ch(i)*rho*0.5*v(j)^2*Sf*c_av/l_Fr(i);
    end
end

surf(delta_o,v,Q);
xlabel('delta [deg]');
ylabel('V [m/s]');
zlabel('Sila [N]');

%% Ciśnienie
Q_0 = Q(300,2);
A1 = 9.5e-3;
A2 = 3.1e-3;

p2 = 100 * 10^5;

p1 = (p2*A2 - Q_0)/A1;

V01 = 0.000496384;
V02 = 0.0000858534;

