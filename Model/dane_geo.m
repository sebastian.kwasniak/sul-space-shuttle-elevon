clear; clc; 
%% Delta(Delta_x)
C = [-184; 188];
B_0 = [14.7; 81.88];

d = sqrt((C(1)-B_0(1))^2+(C(2)-B_0(2))^2);

n = 28;
delta = linspace(-10,20,n);



BC = sqrt((C(1)-B_0(1)*cosd(delta)-B_0(2)*sind(delta)).^2 ...
    +(C(2)+B_0(1)*sind(delta)-B_0(2)*cosd(delta)).^2);
delta_x = BC - d + norm(C-B_0)-200;

%% Dane geometryczne
Ca = 1.0466; %m
lB1 =  0.074;
lB2 = Ca + 0.020;
lA1 = 0.154;
lA2 = Ca + 0.034;
lC1 = 0.212;
lC2 = 0.374;
theta = deg2rad(28); %rad
l1 = lA1 - lB1;
l2 = lA2 - lB2;

S_ref = 1.7; %m^2
S_cs = 0.24; %m^2
rho = 1.225;



save("dane.mat")