clear;
B_0 = [14.7; 81.88];
C = [-394.7; 291.82];
D_0 = [37.8; -37.8];
E_0 = [37.8; 338];
F_0 = [1000; 155-45];
G_0 = [1000;155+55];
delta = linspace(-40,-20,50);


plot(0,0,'o');
axis([-1200 1200 -1200 1200])
hold on;
plot(C(1),C(2),'o');
for i=1:size(delta,2)
    clf;
    plot(0,0,'o');
    axis([-1200 1200 -1200 1200])
    hold on;
    plot(C(1),C(2),'o');
    Rot = [cos(deg2rad(delta(i))),sin(deg2rad(delta(i)));
        -sin(deg2rad(delta(i))),cos(deg2rad(delta(i)))];
    B = Rot*B_0;
    D = Rot*D_0;
    E = Rot*E_0;
    F = Rot*F_0;
    G = Rot*G_0;
    line([B(1) C(1)],[B(2) C(2)]);
    line([B(1) 0],[B(2) 0]);
    line([D(1) E(1)],[D(2) E(2)],'Color','red');
    line([D(1) F(1)],[D(2) F(2)],'Color','red');
    line([E(1) G(1)],[E(2) G(2)],'Color','red');
    line([F(1) G(1)],[F(2) G(2)],'Color','red');
    pause(.1)
end