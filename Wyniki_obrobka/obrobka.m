clc; clear; close all

% schodki = load("schodki\results_in.mat")';
% schodki_in = schodki.ans.Data;
% schodki_in_time = schodki.ans.Time;

schodki_in = [0, -10, -10, -5, -5, 0, 0, 5, 5, 10, 10]';
schodki_in_time = [0, 0, 2, 2, 4, 4, 6, 6, 8, 8, 10]';

schodki = load("schodki\results_out.mat")';
schodki_out = schodki.ans.Data;
schodki_out_time = schodki.ans.Time;


sinus = load("sinus\results_in.mat")';
sinus_in = sinus.ans.Data;
sinus_in_time = sinus.ans.Time;

sinus = load("sinus\results_out.mat")';
sinus_out = sinus.ans.Data;
sinus_out_time = sinus.ans.Time;


stepy = load("stepy\results_in.mat")';
stepy_in = stepy.ans.Data;
stepy_in_time = stepy.ans.Time;

stepy = load("stepy\results_out.mat")';
stepy_out = stepy.ans.Data;
stepy_out_time = stepy.ans.Time;


figure('PaperUnits','normalized','WindowState','maximized')
plot(schodki_in_time, schodki_in, '--', linewidth = 2)
hold on
plot(schodki_out_time, schodki_out, linewidth = 2)
hold off
xlabel('Czas [s]')
ylabel('Wychylenie [Deg]')
grid on
grid minor
legend('Kąt zadany', 'Kąt wyjściowy', 'Location','northeast')
set(gca,'FontSize',30)
saveas(gcf, 'schodki', 'png')



figure('PaperUnits','normalized','WindowState','maximized')
plot(sinus_in_time, sinus_in, '--', linewidth = 2)
hold on
plot(sinus_out_time, sinus_out, linewidth = 2)
hold off
xlabel('Czas [s]')
ylabel('Wychylenie [Deg]')
grid on
grid minor
legend('Kąt zadany', 'Kąt wyjściowy', 'Location','northeast')
set(gca,'FontSize',30)
saveas(gcf, 'sinus', 'png')


figure('PaperUnits','normalized','WindowState','maximized')
plot(stepy_in_time, stepy_in, '--', linewidth = 2)
hold on
plot(stepy_out_time, stepy_out, linewidth = 2)
hold off
xlabel('Czas [s]')
ylabel('Wychylenie [Deg]')
grid on
grid minor
legend('Kąt zadany', 'Kąt wyjściowy', 'Location','northeast')
set(gca,'FontSize',30)
saveas(gcf, 'stepy', 'png')